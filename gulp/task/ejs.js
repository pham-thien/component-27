/**
 * EJSファイルのコンパイル
 * .ejsファイルをコンパイルして.htmlファイルを生成します。
 */

const gulp = require("gulp");
const fs = require("fs");
const w3cjs = require("gulp-w3cjs");
const config = require("../config");
const setting = config.setting;
const $ = require("gulp-load-plugins")(config.loadPlugins);

gulp.task("ejs", () => {
	var json = JSON.parse(fs.readFileSync(setting.html.meta));
	gulp.src([setting.html.src + "**/*.ejs", "!" + setting.html.src + "**/_*.ejs"])
		.pipe($.ejs({ json }, {}, { ext: ".html" }))
		.on("error", $.notify.onError("Error: <%= error.message %>"))
		.pipe($.if(setting.html.validate ,$.htmlhint(".htmlhintrc")))
		.pipe($.plumber())
		.pipe($.if(setting.html.validate ,w3cjs()))
		.pipe($.if(setting.html.validate ,$.htmlhint.failReporter()))
		.pipe(gulp.dest(setting.html.dest))
		.pipe($.browserSync.reload({ stream: true }));
});
